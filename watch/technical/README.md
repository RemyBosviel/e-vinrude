# Veille technique

## Les bonnes adresses

### [La documentation de Symfony](https://symfony.com/doc/current/index.html)

Toujours utile à avoir sous la main pour se mettre à jour sur les composants de Symfony.
Pour l'actualité du framework, voir [le blog de Symfony](https://symfony.com/blog/) (annonce des nouveautés, des différences entre versions, annonce des releases, etc.)

### [Afsy](https://afsy.fr)

Association Francophone des utilisateurs de Symfony. Organise de nombreux événements (Forum PHP / sfPots) et publie plusieurs articles sur Symfony (voir [leur calendrier de l'avent](https://afsy.fr/avent/2019))


### [24 jours de web](https://www.24joursdeweb.fr/)

Un calendrier de l'avent créé en 2012, avec des nouveaux articles chaque année. Beaucoup de retours d'expérience, de discussions autour des différents métiers du web, de l'inclusion, etc. Une resource à parcourir au moins une fois ! 

### [DevDocs](https://devdocs.io/)

Ce site vous permet de regrouper toutes les docs dont vous avez besoin au quotidien en un seul endroit (et vous pouvez tout consulter hors-ligne si besoin).

### [The PHP League](https://thephpleague.com/)

The League of Extraordinary Packages is a group of developers who have banded together to build solid, well tested PHP packages using modern coding standards.
Petits coups de coeur de Rémi : 
- [une lib de gestion des périodes en php](https://period.thephpleague.com/)
- [gérer des fichiers csv sans se prendre le chou](https://csv.thephpleague.com/) 

### [Astuto](https://github.com/riggraz/astuto)

Astuto is a free, open source, self-hosted customer feedback tool. It helps you collect, manage and prioritize feedback from your users.

## Analyse

### [Comprendre le web grâce à HTTPArchive](https://ldevernay.github.io/green/2019/11/13/web_almanac.html)

HTTPArchive récolte en continu des données relatives aux sites web.
Sur un échantillon de plusieurs millions de site, ils sont capables de fournir des tonnes d'indicateurs très intéressants.
Pour la première fois cette année, une analyse de toutes ces données a été mise à disposition sous la forme d’un almanach.

### [Measure and optimize performance and user experience](https://web.dev/metrics/)

Outils de mesure de performance et de qualité de l'expérience utilisateur.

### [Une resource pour bien refactorer](https://refactoring.guru)

Refactoring.Guru makes it easy for you to discover everything you need to know about refactoring, design patterns, SOLID principles, and other smart programming topics.

### [Le guide RGPD pour les développeurs, de la CNIL](https://www.cnil.fr/fr/guide-rgpd-du-developpeur)

Le guide RGPD du développeur offre une première approche des grands principes du RGPD et des différents points d’attention à prendre en compte dans le déploiement d’applications respectueuses de la vie privée des utilisateurs.

## DevOps

### [Automatiser les tests avec gitlab-ci](https://blog.gary-houbre.fr/developpement/gitlab-ci-comment-automatiser-les-tests)

Utiliser l'automatisation de Gitlab pour lancer les tests et d'autres tâches

### [Introduction à Gitlab-ci](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/)

Une explication plutôt détaillée du fichier .gitlab-ci.yml et de son utilisation dans Gitlab. Il explique bien plus en détail que le lien précédent ;) .

## Symfony au détail

### [How to Override Symfony's default Directory Structure](https://symfony.com/doc/current/configuration/override_dir_structure.html)

Parfois, on a besoin/envie de modifier un peu la structure de notre Symfony, voici la doc officielle pour bien le faire

### [Symfony Local Web Server](https://symfony.com/doc/current/setup/symfony_server.html)

Pour utiliser un projet Symfony en local, on peut créer son propre environnement ou utiliser directement le serveur local fourni par Symfony lui-même. À préférer à une installation d'un nginx/apache local ou au serveur php basique.

### [Le calendrier de l'avent 2019 de l'Afsy](https://afsy.fr/avent/2019)

Un ensemble d'articles autour de Symfony. Des astuces, des guides généraux, des bonnes pratiques... Une resource à garder dans un coin !

## Techniques diverses

### [Le SEO, côté développeur](https://www.creativejuiz.fr/blog/referencement-seo/bases-seo-technique-developpeurs)

Les bases du SEO pour les développeurs.

### Gérer ses expressions régulières

- [RegEx Pal](https://www.regexpal.com/) 
- [IHateRegEx](https://ihateregex.io)

Besoin d'aide pour créer / comprendre une expression régulière ? Voici de bons outils de test. Le second vous montre également le fonctionnement de vos expressions régulières sous forme de schéma (pratique pour décomposer). 

### [Les statuts HTTP](https://httpstatuses.com/)

Si vous ne vous souvenez pas des différents statuts http et leur utilité, voilà une bonne resource !

## Front dev

### [De l'utilisation de `console.` dans le code js](https://css-tricks.com/a-guide-to-console-commands/)

Un guide sur les différentes utilisations de `console` pour débugguer du javascript.

### [NuxtJS](https://fr.nuxtjs.org/)

Construisez votre prochaine application Vue.js en toute confiance avec NuxtJS. Un framework open source rendant le développement web simple et puissant.