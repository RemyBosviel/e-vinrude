# Installation Windows

## Git

- [Télécharger la dernière version](https://git-scm.com/download/win)
- Lancer l'installeur. Recommandation : installer Git dans la ligne de commande Windows (c'est une option proposée durant l'installation).

## Docker

On se base sur [la documentation officielle de Docker pour Windows](https://docs.docker.com/docker-for-windows/install/). Attention, pour installer Docker, il faut une version bien à jour de Windows.

- [Télécharger l'installeur de Docker Desktop](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)
- Lancer l'installeur
- Une fois l'installation complète, il est possible qu'il faille mettre à jour le noyau Linux (WSL2) et vous rendre sur cette page : [https://docs.microsoft.com/fr-fr/windows/wsl/wsl2-kernel](https://docs.microsoft.com/fr-fr/windows/wsl/wsl2-kernel). Si vous n'avez pas un message d'erreur vous demandant de l'installer, ne pas s'embêter avec cette étape
- Il est conseillé de faire le tutoriel pour bien vérifier que l'installation s'est faite correctement.

## PhpStorm

- Aller sur [la page de téléchargement de PhpStorm](https://www.jetbrains.com/fr-fr/phpstorm/download/)
- Lancer l'installeur téléchargé
- Lancer PhpStorm et entrer vos identifiants JetBrain
- Configurer ou récupérer votre configuration automatiquement (voir en bas à droite de l'écran, pour la synchronisation)

### Docker dans PhpStorm

En se basant sur [l'aide de PhpStorm sur Docker](https://www.jetbrains.com/help/phpstorm/docker.html), on obtient ça :

- Cliquer sur `Run > Edit Configurations...`
- Cliquer sur le "+" en haut à gauche de la fenêtre qui vient de s'ouvrir.
- Sélectionner `Docker > Docker-compose`
- Donner un nom au Run (Par défaut, `Docker-compose`)

Autres moyens de lancer ça :

- Faire un clic droit sur le fichier `docker-compose.yml` et cliquer sur le bouton "Run"

ou

- Ouvrir le fichier `docker-compose.yml` et cliquer sur les flèches à gauche de `services`

## Gitkraken

- [Télécharger l'installeur pour Windows](https://www.gitkraken.com/download/windows64)
- Lancer l'installeur téléchargé
- Ouvrir Gitkraken, se connecter à un compte (si besoin en créer un, et demander à Rémi pour ajouter le compte à la team)
- Aller dans `File > Preferences > Integrations > GitLab Self-Managed`
- Entrer l'adresse du GitLab (`www.drakolab.fr` pour nous)
- Cliquer sur le bouton de génération de token (sinon, il s'agit d'aller le faire soit-même dans le GitLab) et entrer le token généré par GitLab
- Cliquer sur le bouton de génération et d'ajout automatique de la clé ssh (ce qui vous évite encore une fois de le faire à la main)
- Vous pouvez maintenant cloner et utiliser vos projets :)