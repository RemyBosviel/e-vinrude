# Mise en place de Docker dans un projet Symfony

Tout ce que vous allez lire est une inspiration libre du travail de [Jérémy Graziani](https://www.linkedin.com/in/jgraziani/) ([sa page Malt](https://www.malt.fr/profile/jeremygraziani)) sur un projet commun !

## Pré-requis

- Avoir Docker et Docker-compose installé sur la machine (si besoin, voir [la documentation d'installation d'Ubuntu](ubuntuInstall.html#docker))

## Terminologie

### Docker

Docker est un logiciel de conteneurisation. Il permet d'utiliser des conteneurs (qu'on peut résumer par de micro-machines virtuelles) et fourni un ensemble de commandes pour les gérer, y accéder, les lier entre eux, etc. Pour nous simplifier l'utilisation, nous utiliserons **docker-compose**, un logiciel construit au-dessus de Docker, qui nous en simplifie l'utilisation.

### Image

Une **image Docker** est une version compilée et prête à l'utilisation, avec un ensemble de programme déjà installés.

En général, vous les trouverez sur le [Docker Hub](https://hub.docker.com). Par exemple, vous trouverez nos images php ici : [https://hub.docker.com/repository/docker/drakona/php](https://hub.docker.com/repository/docker/drakona/php)

Une image est déclinée en plusieurs **tags**, différentes variantes préparées. Par exemple, pour des images php, on distinguera les images utilisant php7.2 de php7.4, grâce aux tags.

### Conteneur

Un **conteneur** est une instance d'une image. En clair, pour une même image, vous pouvez avoir plusieurs conteneurs lancés (pour des projets différents par exemple).
Les fichiers d'un conteneur sont remis à zéro à chaque démarrage (il se "remet à zéro" à chaque démarrage). Pour sauvegarder des fichiers, il va falloir utiliser des **volumes** 

### Volumes

Un volume est un dossier partagé entre votre machine (la machine hôte) et le conteneur. Ce dossier peut être un dossier précis de votre machine, ou un dossier que vous allez demander à Docker de conserver (il le rangera alors par lui-même).

## Utilisation de Docker-compose

Pour simplifier l'utilisation de Docker et devoir démarrer et relier nos conteneurs entre eux par nous-mêmes, nous utilisons un programme nommé Docker-compose. Comme son nom l'indique, il va nous permettre de composer notre environnement Docker.

Prenons un exemple réel directement et détaillons le fonctionnement : 
 
 ```yaml
version: "3"

services:
    app:
        build: docker/php
        depends_on:
            - db
        volumes:
            - .:/srv
            - "~/.composer:/.composer"

    nginx:
        build: docker/nginx
        depends_on:
            - app
        volumes:
            - .:/srv

    assets:
        image: node:10
        working_dir: /srv
        environment:
            - NODE_ENV=development
            - LOGGY_STACKS=1
        volumes:
            - .:/srv

    db:
        image: mysql:5.7
        command: --max-allowed-packet=6710886400 --default-authentication-plugin=mysql_native_password
        environment:
            MYSQL_ROOT_PASSWORD: pass
            MYSQL_DATABASE: site
            MYSQL_USER: root
            MYSQL_PASSWORD: pass
        volumes:
            - db-data:/var/lib/mysql

    phpmyadmin:
        image: phpmyadmin/phpmyadmin:latest
        depends_on:
            - db
        links:
            - db:db
        ports:
            - 8080:80
        environment:
            MYSQL_ROOT_PASSWORD: pass
            MYSQL_USER: root
            MYSQL_PASSWORD: pass

    adminer:
        image: adminer:latest
        ports:
            - '8081:8080'

    mailhog:
        image: mailhog/mailhog
        logging:
            driver: 'none'
        ports:
            - '8025:8025'
            - '1025:1025'

volumes:
    db-data: {}

```

```yaml
version: "3"
```

Correspond à la version de Docker-compose que nous utilisons. Selon les versions, la syntaxe a un peu évolué et plus ou moins d'éléments sont disponibles. Il est donc très important de le préciser ;) . 

```yaml
services:
```

Dans cette section, nous déclarons les conteneurs que nous allons vouloir utiliser. Chaque sous-section détaille l'image à utiliser et la configuration associée.

```yaml
    app:
        build: docker/php
        depends_on:
            - db
        volumes:
            - .:/srv
            - "~/.composer:/.composer"
```

Pour la déclaration du service app (notre service principal), il faut distinguer 3 éléments :
- `build` indique que nous allons compiler (build) notre conteneur à partir d'instructions se trouvant dans le dossier `docker/php`. L'utilisation d'un fichier `Dockerfile` est expliquée un peu plus loin ;) .
- `depends_on` indique un service qui doit être lancé avant celui-ci. En l'occurrence, la base de donnée doit être prête pour que ce conteneur démarre.
- `volumes` indique les dossiers qui seront partagés entre la machine hôte (votre ordinateur) et le conteneur. Ici, on indique qu'on partage le dossier courant `.` de la machine hôte et qu'il sera disponible dans le dossier `/srv` du conteneur. On partage également le dossier `~/.composer` (configuration et cache de composer de l'utilisateur courant de la machine hôte) dans le dossier `/.composer` du conteneur

```yaml
    db:
        image: mysql:5.7
        command: --max-allowed-packet=6710886400 --default-authentication-plugin=mysql_native_password
        environment:
            MYSQL_ROOT_PASSWORD: pass
            MYSQL_DATABASE: site
            MYSQL_USER: root
            MYSQL_PASSWORD: pass
        volumes:
            - db-data:/var/lib/mysql
```

Dans cet exemple, on utilise directement une image (rien à compiler, elle sera automatiquement téléchargée et utilisée). `image: mysql:5.7` indique qu'on utilise l'[image officielle mysql (disponible sur Docker Hub)](https://hub.docker.com/_/mysql) avec le tag `5.7`, ce qui correspond à un serveur mysql, en version 5.7 (évident, une fois qu'on le sait, n'est-ce pas ? ;) ).

```yaml
        command: --max-allowed-packet=6710886400 --default-authentication-plugin=mysql_native_password
```

Ici, on indique des paramètres supplémentaire à la commande lancée par le conteneur. En clair, on va dire à mysql d'accepter de plus gros fichiers et de gérer l'authentication avec le plugin `mysql_native_password`, qui ne demande pas des mots de passe trop complexes (nous voulons mettre `pass` comme mot de passe, ce qui n'est pas une bonne sécurité en prod, mais largement suffisant pour nos dockers).

```yaml
        environment:
              MYSQL_ROOT_PASSWORD: pass
              MYSQL_DATABASE: site
              MYSQL_USER: root
              MYSQL_PASSWORD: pass
```

Les variables d'environnement à transmettre à l'image. Pour la plupart des images, vous pouvez ainsi passer des options diverses et variées et vous en trouverez la liste sur la page Docker Hub de l'image (surtout pour les images officielles).

```yaml
        volumes:
            - db-data:/var/lib/mysql
...

volumes:
    db-data: {}
```

Ici, on déclare un volume `db-data` que Docker va gérer lui-même et qui va nous servir à conserver nos fichiers de base de données d'une utilisation à l'autre de notre conteneur.

Quelques mentions honorables : 

```yaml
        links:
            - db:db
```

Ici, nous allons lier le service db, défini dans notre docker-compose, au service db attendu par notre service (ici phpmyadmin). C'est un paramètre attendu par l'image et vous serez en général prévenu, dans la doc sur Docker Hub, si vous en avez besoin.

```yaml
        ports:
            - 8080:80
```

Ici, on modifie le port sur lequel le service est disponible. Dans ce cas, notre port `8080` va appeler le port `80` du service (ici phpmyadmin, qui contient un serveur apache).

## Créer ses propres images Docker : le Dockerfile

Nous avons une collection de Dockerfiles pour gérer la plupart des cas courants dans [notre repository dédié](https://www.drakolab.fr/drakona/interne/dockerfiles).

### Un Dockerfile simple : Nginx

Nous allons avoir 4 fichiers dans un dossier `docker/nginx` de notre projet :

- `Dockerfile` le fichier central, nécessaire pour build notre conteneur
- `nginx.conf` pour la configuration générale de notre nginx
- `symfony_dev.conf` et `symfony_prod.conf` les déclarations de nos *serveurs* de dev et de prod, chacun sur des ports différents, pour nous permettre de tester dans les deux environnements

#### Dockerfile

```dockerfile
FROM nginx:1.19.2-alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY symfony_dev.conf /etc/nginx/conf.d
COPY symfony_prod.conf /etc/nginx/conf.d
RUN rm -f /etc/nginx/conf.d/default.conf

WORKDIR /srv

EXPOSE 80
EXPOSE 443

CMD ["nginx"]
```

```dockerfile
FROM nginx:1.19.2-alpine
```

On indique l'image de base que l'on utilise. Ici, on se sert de l'image officielle de nginx, en version 1.19.2, basée sur une [Linux Alpine](https://alpinelinux.org/).

```dockerfile
COPY nginx.conf /etc/nginx/nginx.conf
COPY symfony_dev.conf /etc/nginx/conf.d
COPY symfony_prod.conf /etc/nginx/conf.d
RUN rm -f /etc/nginx/conf.d/default.conf
```

On gère les fichiers de configuration présents dans notre image : on met notre fichier de configuration, nos 2 serveurs et on supprime la configuration par défaut de Nginx.

```dockerfile
WORKDIR /srv

EXPOSE 80
EXPOSE 443

CMD ["nginx"]
```

- `WORKDIR` indique le dossier de travail de l'image, le dossier qui va être partagé avec notre image principale (`app`)
- `EXPOSE` indique les ports utilisables
- `CMD` est la commande que l'image va faire tourner tant qu'elle est active (ici, on lance le serveur Nginx, qui va attendre nos connexions)

Passons ensuite à nos fichiers de configuration (que je ne détaille pas, ce n'est pas le sujet ici ;P) : 

#### nginx.conf

```nginx
pid /run/nginx.pid;

worker_processes 4;
daemon off;

events {
        worker_connections 768;
        multi_accept on;
}

http {
        client_max_body_size 20M;
        real_ip_header X-Forwarded-For;
        real_ip_recursive on;

        ##
        # Basic Settings
        ##

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 65;
        types_hash_max_size 2048;
        server_tokens off;

        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;

        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        ##
        # SSL Settings
        ##

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;

        ##
        # Logging Settings
        ##

        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;

        ##
        # Gzip Settings
        ##

        gzip on;
        gzip_disable "msie6";

        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 6;
        gzip_buffers 16 8k;
        gzip_http_version 1.1;
        gzip_types text/css text/javascript text/xml text/plain text/x-component application/javascript application/x-javascript application/json application/xml  application/rss+xml font/truetype application/x-font-ttf font/opentype application/vnd.ms-fontobject image/svg+xml;

        ##
        # Virtual Host Configs
        ##

        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/sites-enabled/*;
}
```

#### symfony_dev.conf

```nginx
server {
    listen 80;
    server_name _;
    root /srv/public;

    location / {
        # try to serve file directly, fallback to index.php
        try_files $uri /index.php$is_args$args;
    }

    # optionally disable falling back to PHP script for the asset directories;
    # nginx will return a 404 error when files are not found instead of passing the
    # request to Symfony (improves performance but Symfony's 404 page is not displayed)
    # location /bundles {
    #     try_files $uri =404;
    # }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass app:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        # optionally set the value of the environment variables used in the application
        # fastcgi_param APP_ENV prod;
        # fastcgi_param APP_SECRET <app-secret-id>;
        # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/index.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/dev_error.log;
    access_log /var/log/nginx/dev_access.log;
}

```

#### symfony_prod.conf

```nginx
server {
    listen 8085;
    server_name _;
    root /srv/public;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass app:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;
    }

    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/prod_error.log;
    access_log /var/log/nginx/prod_access.log;
}

```

Quelques éléments à noter toutefois : 

```nginx
    listen 8085;
    server_name _;
    root /srv/public;
```

Ici, on indique qu'on va répondre quelque que soit le domaine demandé, du moment qu'il s'agit du port 8085, et notez bien le root, qui correspond au `WORKDIR` de tout à l'heure.

```nginx
        fastcgi_pass app:9000;
```

Pour communiquer avec notre php-fpm, on met le nom de notre service, déclaré dans notre docker-compose et Docker se débrouille pour lier le tout.

### Dockerfile PHP

Partons d'une machine php-fpm officielle standard (qui est une machine debian). On y ajoute ensuite les extensions php dont on a besoin, composer, php-cs-fixer et autres outils utiles. On met ensuite à jour le php.ini (configuration de php), le www.conf (configuration du démon de php-fpm) et la commande principale.

On a donc de nouveau 4 fichiers : 
- `Dockerfile` notre fichier principal
- `php.ini` la configuration de php
- `start.sh` notre commande principale exécutée par le conteneur
- `www.conf` la configuration de php-fpm

#### Dockerfile

```dockerfile
FROM php:7.2-fpm

RUN apt-get update \
    && apt-get -y install \
       zip zlib1g-dev libzip-dev \
       wget gnupg \
       git

RUN docker-php-ext-configure mysqli \
    && docker-php-ext-install pdo pdo_mysql mysqli \
    && docker-php-ext-install opcache zip

RUN apt-get update && apt-get install -y libmagickwand-dev --no-install-recommends && rm -rf /var/lib/apt/lists/* \
    && printf "\n" | pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-install exif

# PHP intl extension
RUN apt-get install -y libicu-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

# PHP gd extension
RUN apt-get update && \
    apt-get install -y zlib1g-dev \
    && docker-php-ext-install gd

RUN mkdir ~/.ssh/ && ssh-keyscan github.com >> ~/.ssh/known_hosts

# Composer installation
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer \
    && composer global require hirak/prestissimo --no-plugins --no-scripts \
    && chmod +x /usr/local/bin/composer \
    && ln -snf /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Php CS Fixer installation
RUN wget http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar -O php-cs-fixer.phar \
    && mv php-cs-fixer.phar /usr/local/bin/php-cs-fixer \
    && chmod +x /usr/local/bin/php-cs-fixer

# XDebug
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug \
    && docker-php-ext-install sysvsem

RUN docker-php-ext-install calendar && docker-php-ext-configure calendar

# Install backward-compatible APC (for prod env)
RUN pecl install apcu \
    && pecl install apcu_bc \
    && docker-php-ext-enable apcu --ini-name 10-docker-php-ext-apcu.ini \
    && docker-php-ext-enable apc --ini-name 20-docker-php-ext-apc.ini

# Redirect internal port php:80 to nginx:80 (used by pdf exports that, by default, makes request on the wrong host)
RUN apt -y install socat

ADD start.sh /usr/local/bin
COPY php.ini /usr/local/etc/php
COPY www.conf /usr/local/etc/php-fpm.d/www.conf

WORKDIR /srv

EXPOSE 9000

CMD ["start.sh"]

```

```dockerfile
RUN apt-get update \
    && apt-get -y install \
       zip zlib1g-dev libzip-dev \
       wget gnupg \
       git
```

Installation de logiciels requis pour des extensions ou des logiciels par la suite (on installe surtout zip, wget et gnupg qui vont nous permettre de télécharger/installer certains programmes) et git (nécessaire pour installer des paquets *via* composer). Globalement, ces installations-là servent à tout le reste et ne sont pas liées à une extension/un programme en particulier.

```dockerfile
RUN docker-php-ext-configure mysqli \
    && docker-php-ext-install pdo pdo_mysql mysqli \
    && docker-php-ext-install opcache zip
```

Installation d'extensions php : pdo, MySQL, opcache et zip

```dockerfile
RUN apt-get update && apt-get install -y libmagickwand-dev --no-install-recommends && rm -rf /var/lib/apt/lists/* \
    && printf "\n" | pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-install exif
```

Là, on installe imagick et son extension php.

```dockerfile
RUN mkdir ~/.ssh/ && ssh-keyscan github.com >> ~/.ssh/known_hosts
```

On transmet nos clés ssh au conteneur Docker.

```dockerfile
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer \
    && composer global require hirak/prestissimo --no-plugins --no-scripts \
    && chmod +x /usr/local/bin/composer \
    && ln -snf /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Php CS Fixer installation
RUN wget http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar -O php-cs-fixer.phar \
    && mv php-cs-fixer.phar /usr/local/bin/php-cs-fixer \
    && chmod +x /usr/local/bin/php-cs-fixer
```

Installation de composer et php-cs-fixer dans le conteneur pour être utilisés directement dedans.

```dockerfile
ADD start.sh /usr/local/bin
COPY php.ini /usr/local/etc/php
COPY www.conf /usr/local/etc/php-fpm.d/www.conf

WORKDIR /srv

EXPOSE 9000

CMD ["start.sh"]
```

On ajoute nos fichiers de configuration et notre start.sh dans le conteneur, on précise le `WORKDIR` (tant qu'à faire, autant utiliser `/srv` pour tout le monde), on expose le port 9000 (petit rappel de la config nginx) et on dit au conteneur d'utiliser notre commande `start.sh` au démarrage.

#### php.ini

```ini
[PHP]
post_max_size = 256M
upload_max_filesize = 256M
max_file_uploads = 20
memory_limit = -1
max_execution_time = 120
date.timezone = Europe/Paris
opcache.enable = 1
opcache.memory_consumption = 256
opcache.max_accelerated_files = 20000
opcache.fast_shutdown = 1
realpath_cache_size = 4096K
realpath_cache_ttl = 600
```

À adapter selon vos besoins, mais on se met dans de bonnes conditions pour travailler ;) (on peut envoyer des fichiers de 256M, on règle notre timezone, etc.)

#### start.sh

```bash
#!/usr/bin/env bash

echo -e "\e[32m------------ Installing required packages via Composer ------------\e[0m\n"

composer install --no-interaction | sed -e "s/^/`printf "\e[1;95m"`[COMPOSER]`printf "\e[0m"`\t/"

echo -e "\e[32m------------            Starting php-fpm...            ------------\e[0m\n"

echo "Project is accessible here:" \
&& echo "• http://www.site.test:8000 (dev)"
# Silently cleaning prod cache (it's slow and prod is rarely used, so we run it in the background)
php bin/console    cache:clear  --env=prod --quiet \
&& php bin/console assets:install --env=prod --quiet \
&& echo "• http://www.site.test:8085 (prod)" &

php-fpm -R 2>&1 | sed -e "s/^/`printf "\e[1;96m"`[PHP]`printf "\e[0m"`\t/"
```

Dans ce fichier, on précise que dès que les éléments précédents sont prêts, on lance l'installation de composer, un cache:clear de l'environnement de prod, un assets:install, puis on démarre php-fpm (la dernière ligne). Cette dernière commande va continuer à tourner en continu et gérer les connexions envoyées par nginx.

#### www.conf

 ```ini
[global]
daemonize=no

[www]
user=root
group=root

listen=nginx:9000

pm=dynamic
pm.max_children=100
pm.start_servers=6
pm.min_spare_servers=2
pm.max_spare_servers=6
pm.max_requests=500

; some extra padding, to align [PHP] output with [STRIPE] output
access.format = "           %{%H:%M:%S}t [%s] %m %{REQUEST_SCHEME}e://%{HTTP_HOST}e%{REQUEST_URI}e (%ds)"
 ```

Ici, on notera surtout le `listen=nginx:9000` qui attend les connexions du conteneur `nginx` sur le port 9000.

