# Jenkins files

```
@Library('test-library@master') _

import com.test.Utilities

node('master') {
    try{
        timeout(15) {
            def utilities = new Utilities(this, "${ENVIRONMENT}", "${env.WORKSPACE}")
            utilities.setProject('test-front')

            wrap([$class: 'BuildUser']) {
                // https://wiki.jenkins-ci.org/display/JENKINS/Build+User+Vars+Plugin variables available inside this block
                currentBuild.displayName = "${ENVIRONMENT} - ${BRANCH}"
                currentBuild.description = "launched by : " + env.BUILD_USER
            }

            stage('checkout') {
                checkoutProject 'test-front-v2', "${BRANCH}"
            }

            stage('build env') {
                if ("${ENVIRONMENT}".contains("TRN-LAN")) {
                    sh '''#!/bin/bash
                        cp application/.env.trn_lan application/.env
                    '''
                }
                if ("${ENVIRONMENT}".contains("TRN-COM")) {
                    sh '''#!/bin/bash
                        cp application/.env.trn_com application/.env
                    '''
                }
                if ("${ENVIRONMENT}".contains("QAT-COM")) {
                    sh '''#!/bin/bash
                        cp application/.env.qat_com application/.env
                    '''
                }
                if ("${ENVIRONMENT}".contains("QAT-LAN")) {
                    sh '''#!/bin/bash
                        cp application/.env.qat_lan application/.env
                    '''
                }
                if ("${ENVIRONMENT}".contains("INT-COM")) {
                    sh '''#!/bin/bash
                        cp application/.env.int_com application/.env
                    '''
                }
                if ("${ENVIRONMENT}".contains("INT-LAN")) {
                    sh '''#!/bin/bash
                        cp application/.env.int_lan application/.env
                    '''
                }
           }

            if (!utilities.isAlreadyBuild() || params.FORCE_REBUILD) {
                stage('install dependencies') {
                    node ('slavephp73') {
                        if ("${ENVIRONMENT}".contains("QAT-LAN") || "${ENVIRONMENT}".contains("TRN-LAN")) {
                            installDependencies 'composer', utilities.getWorkspace(), '-a -n --no-scripts'
                        } else {
                            installDependencies 'composer', utilities.getWorkspace(), '-a -n --no-dev --no-scripts'
                        }
                    }

                     installDependencies 'javascript', utilities.getWorkspace()
                }

                stage('build') {
                    sh "sh generate-version-yaml.sh ${BRANCH}"
                    sh "yarn run encore production --display-error-details"
                }

                // test only when deploying on staging or prod
                if (!utilities.canBeDeployed()) {
                    stage('tests') {
                        node ('slavephp73') {
                            launchTest {
                              workspace = utilities.getWorkspace()
                              type = 'phpunit'
                              arguments = '--group git-pre-push,git-livingit,git-services,block-documentation -d error_reporting=22527 --configuration app/phpunit.xml.dist'
                            }
                        }
                    }
                }

                stage('archive') {
                    buildArchive {
                        type = 'zip'
                        archivePath = utilities.getArchivePath()
                        files = '.'
                        chdir = 'application'
                        excludeFiles = ['application/var/cache/*', 'application/var/log/*', 'application/var/translations/*']
                    }
                }
            }

            if (utilities.canBeDeployed()) {
                stage('deploy') {
                    deploy {
                        archive = utilities.getArchiveName()
                        target = utilities.getDeployTargetMachine()
                        application = utilities.getDeployApplication()
                    }
                }
            }
        }
    } catch (e) {
        currentBuild.result = 'FAILURE'
        // Since we're catching the exception in order to report on it,
        // we need to re-throw it, to ensure that the build is marked as failed
        throw e
    } finally {
        def currentResult = currentBuild.result ?: 'SUCCESS'
        def buildBranch = "${params.BRANCH}"
        def buildEnv = "${params.ENVIRONMENT}"

        slackNotifier {
            buildResult = currentResult
            projectName = 'test-front'
            branch = buildBranch
            target = buildEnv
        }
    }
}
```

## JenkinsFileTest

```
@Library('test-library@feat/add-simple-phpunit') _

import com.test.Utilities
def utilities
node('master') {
    timeout(15) {
        utilities = new Utilities(this, 'TEST', "${env.WORKSPACE}")
        utilities.setProject('test-front')

        // change branch is for pr and other one is for branches
        def BRANCH_TO_BUILD = (env.CHANGE_BRANCH == null) ? env.BRANCH_NAME : env.CHANGE_BRANCH

        println "WORKSPACE ${env.WORKSPACE}"
        println "Source Branch: ${BRANCH_TO_BUILD}"

        stage('checkout') {
            checkoutProject 'test-front-v2', "${BRANCH_TO_BUILD}"
        }

        stage('build env') {
            sh '''#!/bin/bash
                cp application/.env.qat_com application/.env
            '''
        }

        stage('install dependencies') {
            node ('slavephp73') {
                installDependencies 'composer', utilities.getWorkspace(), '-n --no-scripts'
            }

             installDependencies 'javascript', utilities.getWorkspace()
        }
        stage('build') {
            sh "sh generate-version-yaml.sh ${BRANCH_TO_BUILD}"
            sh "yarn run encore production"
        }

        stage('tests') {
            node ('slavephp73') {
                stage('phpunit test') {
                    launchTest {
                        workspace = utilities.getWorkspace()
                        type = 'simple-phpunit'
                        arguments = '-d error_reporting=22527 --configuration phpunit.xml.dist'
                    }
                }

                stage('quality') {
                    launchTest {
                      workspace = utilities.getWorkspace()
                      type = 'php-cs-fixer'
                      arguments = 'fix --dry-run --using-cache=no --verbose --diff'
                    }
                    launchTest {
                      workspace = utilities.getWorkspace()
                      type = 'phpstan'
                      arguments = 'analyse --level=1'
                    }
                }
            }

            launchTest {
                workspace = utilities.getWorkspace()
                type = 'javascript'
                arguments = 'test'
            }

            launchTest {
                workspace = utilities.getWorkspace()
                type = 'javascript'
                arguments = 'amp-validator'
            }
        }
        stage('deployment feature qat') {
            try {
                buildArchive {
                    type = 'zip'
                    archivePath = utilities.getArchivePath()
                    files = '.'
                    excludeFiles = ['application/var/cache/*', 'application/var/log/*', 'application/var/translations/*']
                    chdir = 'application'
                }
                deployStrategy(utilities)
            } catch(e) {
                currentBuild.result = 'FAILURE'
                // Since we're catching the exception in order to report on it,
                // we need to re-throw it, to ensure that the build is marked as failed
                throw e
            } finally {
                def featureDir = utilities.getBranchForFeatureDirectory()

                def currentResult = currentBuild.result ?: 'SUCCESS'
                def buildBranch = "FEAT : http://${featureDir}.www.test.com.qat.local"
                def buildEnv = "TEST"

                slackNotifier {
                    buildResult = currentResult
                    projectName = 'test-front-v2'
                    branch = buildBranch
                    target = buildEnv
                }
            }

        }
        stage('cleanup') {
            cleanWs()
            dir("${env.WORKSPACE}@tmp") {
              deleteDir()
            }
            dir("${env.WORKSPACE}@script") {
              deleteDir()
            }
            dir("${env.WORKSPACE}@script@tmp") {
              deleteDir()
            }
            dir("${env.WORKSPACE}@libs") {
              deleteDir()
            }
        }
    }
}
```
